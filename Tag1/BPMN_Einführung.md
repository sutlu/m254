# BPMN Einführung

## Start und Ende

Jeder Prozess enthält Start- Zwischen- und Endereignisse. Der Start wird mit einem Kreis aus einem dünnen Strich das Ende mit einem Kreis aus einem Dünnenstrich und die Zwischenereignisse doppelt Umrandet.

![StarEndEreignis](img/BPMN_Einführung/StartEndereignis.png)

## Aktivitäten

Werden als Rechteck  mit runden Ecken dargestellt.

![Aktivität](img/BPMN_Einführung/Aktivitäten.png)

## Konektoren

Am häufigst verwendeten Konnektoren sind Sequenzflüsse (einfacher Pfeil).

## Gateways

Rautenförmige Elemente erlauben Unterteilung oder Zusammenflüsse von Sequenzflüssen.

![Gateways](img/BPMN_Einführung/Gateways.png)

### exklusive Gateways/XOR

Dient zur modellierung alternativer Pfade --> Verzeigung. Entweder Oder Entscheidung. Pfade können mit oder ohne erneutem Gateway wideder zusammengeführt werden.

### parallele Gateways/AND

Alle ausgehenden Sequenzflüsse werden auf einmal Aktiviert (paralell). Pfade müssen mit erneutem Gateway wideder zusammengeführt werden.

### inklusives Gateway/OR

Es werden ein oder mehrere Sequenzflüsse fortesetzt. Es muss aber mindestens eine Option gewählt werden, sonst wird der Prozess nicht weitergeführt. Bei der Zusammenführung wird auf alle Aktivitäten gewartet, welche aktiviert wurden. Deshalb kann ein AND auch eine Synchronisierende Wirkung haben.

### ereignisbasierte Gateways

Verhält sich ähnlich wie ein exklusives Gateway. Der Unterschied ist darin, dass es durch ein Ereigniss aktiviert wird und nicht durch eine Aktivität.

![EreignisbasiertesGateway](img/BPMN_Einführung/EreignisbasiertesGatewway.png)

Der Pfad hängt davon ab, welches Ereignis zuerst eintrifft.

### ereignisbasierte exklusive Gateways

Grundsätzlich gleiche bedeutung wie ereignisbasiertes Gateway. Steht aber am Anfang eines Prozesses. Darf keine eingehenden Sequenzflüsse haben.

![ereigbasexklGateway](img/BPMN_Einführung/ereigbasexklGateway.png)

### ereignisbasierte parallele Gateways

Wie ereignisbasierte exklusive Gateway, startet aber erst, wenn alle nachfolgenden Ereignisse erfüllt sind.

### komplexe Gateways

Wird verwendet wen mit anderen Gateways nicht lösbar. Wird mit Textanmerkung konkretisiert.

![komplexesGateway](img/BPMN_Einführung/komplexesGateway.png)

<img src="img/BPMN_Einführung/komplexesGateway.png" alt="drawing" width="50px"/>

## Datenobjekte

Ein Prozess generiert oder benötigt während der Abwicklung meisst daten. Diese werden mit einem Blatt dargestellt und mit einem gestrichelten Pfeil mit der dazugehörigen Aktivität verknüpft

![Datenobjekt](img/BPMN_Einführung/DatenObjekt.png)

## Pools und Lanes

### Pools

Definieren Teilnehmer innerhalb einer Kollaboration z.B. Organisationen.

### Lanes

Sind nicht genauer Definiert können also vom Ersteller des BPMN selber definiert werden. Oft werden Sie verwendet um einzelne Abteilungen darzustellen.

![PoolsLanes](img/BPMN_Einführung/PoolsLanes.png)
